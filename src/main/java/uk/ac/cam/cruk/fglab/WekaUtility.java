package uk.ac.cam.cruk.fglab;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.gui.ShapeRoi;


import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.converters.ArffLoader;
import weka.core.converters.ConverterUtils.DataSink;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.SerializationHelper;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.getAttributeFromRandomForest;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.Classifier;


public interface WekaUtility {

	public static final String[] statisticalFeatureString = {"Mean", "StdDev", "CV", "Skewness", "Kurtosis"};
		//statisticalFeatureString.add("Mean");
		//statisticalFeatureString.add("StdDev");
		//statisticalFeatureString.add("CV");
		//statisticalFeatureString.add("Skewness");
		//statisticalFeatureString.add("Kurtosis");
	
	public static Classifier getClassifier (
			String modelPath
			) throws Exception {
		return (Classifier) SerializationHelper.read(modelPath);
	}
	public static boolean getPredictionResult(
			String modelPath,
			Instances data
			) {
		
		Classifier rf;
		try {
			rf = (Classifier) SerializationHelper.read(modelPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		int classIdx=data.numAttributes()-1;
		data.setClassIndex(classIdx);
		double label = 0;
		try {
			label = rf.classifyInstance(data.instance(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		data.instance(0).setClassValue(label);
		//IJ.log("debug: WekaUtility, data class value: " + data.instance(0).toString(classIdx));
		return (data.instance(0).toString(classIdx).equals("yes"));

	}
	
	public static ArrayList<Integer> getFeatureChannelsFromModel (
			Classifier cls
			) throws Exception {
		ArrayList<String> featureAttributes = getFeatureAttributeFromModel(cls);
		ArrayList<Integer> numChannels = new ArrayList<Integer>();
		for (String feature : featureAttributes) {
			//println(cls.m_data.attribute(i).name());
			int numC = Integer.valueOf(feature.split("_")[0].replaceAll("\\D+",""));
			if (!numChannels.contains(numC)) numChannels.add(numC);
		}
		return numChannels;
	}
	
	public static ArrayList<Integer> getObjFeatureFromModel (
			Classifier cls
			) throws Exception {
		ArrayList<String> featureAttributes = getFeatureAttributeFromModel(cls);
		ArrayList<Integer> numFeatures = new ArrayList<Integer>();
		for (String feature : featureAttributes) {
			int statFeature = 0;
			String objFeature = feature.split("_")[1];
			//IJ.log("debug: WekaUtility: 93: objFeature: " + objFeature);
			statFeature = Arrays.asList(statisticalFeatureString).indexOf(objFeature) + 1;
			//IJ.log("debug: WekaUtility: 95: statFeature: " + statFeature);
			/*
			switch (objFeature) {
				case "Mean": 
					statFeature = 1; break;
				case "StdDev": 
					statFeature = 2; break;
				case "CV":
					statFeature = 3; break;
				case "Skewness":
					statFeature = 4; break;
				case "Kurtosis":
					statFeature = 5; break;
				default:
					continue;
			}*/
			if (statFeature==0) continue;
			if (!numFeatures.contains(statFeature)) numFeatures.add(statFeature);
		}
		//IJ.log("debug: WekaUtility: 113: numFeatures: " + numFeatures);
		return numFeatures;
	}
	
	public static ArrayList<String> getClassAttributeFromModel (
			Classifier cls
			) throws Exception {
		if (cls instanceof RandomForest) {
			RandomForest rf = (RandomForest) cls;
			Instances data = getAttributeFromRandomForest.getAttributeFromRandomForest(rf); //hacky way to get protected member m_data from weka.classifier.meta.bagging...
			int numClasses = data.numClasses();
			ArrayList<String> classAttributes = new ArrayList<String>();
			for (int i=0; i<numClasses; i++) {
				classAttributes.add(data.classAttribute().value(i));
			}
			return classAttributes;
		}
		return null;
	}
	
	public static ArrayList<String> getFeatureAttributeFromModel (
			Classifier cls
			) throws Exception {
		if (cls instanceof RandomForest) {
			RandomForest rf = (RandomForest) cls;
			Instances data = getAttributeFromRandomForest.getAttributeFromRandomForest(rf);	//hacky way to get protected member m_data from weka.classifier.meta.bagging...
			int numAttributes = data.numAttributes() - 1;
			ArrayList<String> featureAttributes = new ArrayList<String>();
			for (int i=0; i<numAttributes; i++) {
				featureAttributes.add(data.attribute(i).name());
			}
			return featureAttributes;
		}
		return null;
	}
	
	public static int getNumInstanceFromModel (
			Classifier cls
			) throws Exception {
		//if (cls instanceof RandomForest) {
			RandomForest rf = (RandomForest) cls;
			Instances data = getAttributeFromRandomForest.getAttributeFromRandomForest(rf); //hacky way to get protected member m_data from weka.classifier.meta.bagging...
			return data.numInstances();
		//}
		//return 0;
	}
	
	
	public static boolean saveTrainingDataAndModel(
			
			) {
		
		return true;
	}
	
	public static Instances createTestingInstance (
			ImagePlus imp,
			Roi roi,
			ArrayList<String> objectClasses,
			ArrayList<Integer> channels,
			ArrayList<Integer> statisticalFeatures
			) {
		
			ArrayList<Attribute>	atts;
		    Instances			data;
		    double[]			vals;
		    
		    // 1. set up attributes
		    atts = new ArrayList<Attribute>();	    
		    for (Integer channel : channels) {
		    	String featureName = "C" + String.valueOf(channel) + "_";
		    	for (Integer statisticalFeature : statisticalFeatures) {
		    		featureName += statisticalFeatureString[statisticalFeature-1];
		    		atts.add(new Attribute(featureName));
		    	}
		    }
			atts.add(new Attribute("class", objectClasses));
		    // 2. create Instances object
		    data = new Instances("ObjectSegmentation", atts, 0);

			imp.setPositionWithoutUpdate(roi.getCPosition(), roi.getZPosition(), roi.getTPosition());
			imp.setRoi(roi, false);
			
			vals = new double[data.numAttributes()];
			
			int idx = 0;
		
			for (Integer c : channels) {
				imp.setC(c);
				for (Integer f : statisticalFeatures) {
					switch (f) {
						case 1:
							vals[idx] = imp.getRawStatistics().mean;
							break;
						case 2:
							vals[idx] = imp.getRawStatistics().stdDev;
							break;
						case 3:
							vals[idx] = imp.getRawStatistics().stdDev/imp.getRawStatistics().mean;
							break;
						case 4:
							vals[idx] = imp.getAllStatistics().skewness;
							break;
						case 5:
							vals[idx] = imp.getAllStatistics().kurtosis;
							break;
						default:
							continue;
					}
					idx++;
				}
			}
			
			vals[vals.length-1] = Utils.missingValue();
			data.add(new DenseInstance(1.0, vals));
			return data;
	}
	
	
	
	public static Instances createInstance (
			ImagePlus imp,
			Roi roi,
			ArrayList<String> objectClasses,
			ArrayList<Integer> channels,
			ArrayList<Integer> statisticalFeatures,
			int classLabel
			) {
		
	    ArrayList<Attribute>	atts;
	    Instances			data;
	    double[]			vals;

		// parse input image
		int n = imp.getCurrentSlice();
		int numC = imp.getNChannels();
		//imp.hide();
		
		imp.getCalibration().pixelWidth = 1.0; imp.getCalibration().pixelHeight = 1.0;
		imp.getCalibration().setUnit("pixel");

	    // 1. set up attributes
	    atts = new ArrayList<Attribute>();	    
	    for (Integer channel : channels) {
	    	String featureName = "C" + String.valueOf(channel) + "_";
	    	for (Integer statisticalFeature : statisticalFeatures) {
	    		featureName += statisticalFeatureString[statisticalFeature-1];
	    		atts.add(new Attribute(featureName));
	    	}
	    }
		atts.add(new Attribute("class", objectClasses));
	    // 2. create Instances object
	    data = new Instances("ObjectSegmentation", atts, 0);

		//roi = rm.getRoi(r).clone();
		//imp.setPositionWithoutUpdate(roi.getCPosition(), roi.getZPosition(), roi.getTPosition());
		//imp.setRoi(roi, false);
		//impCrop = new Duplicator().run(imp, 1, imp.nChannels, 1, imp.nSlices, imp.getT(), imp.getT() );
		//imp.deleteRoi();
		roi = cropRoi(imp, roi);
		//roi.setLocation(0,0);
		imp.setPositionWithoutUpdate(roi.getCPosition(), roi.getZPosition(), roi.getTPosition());
		imp.setRoi(roi, false);
		vals = new double[data.numAttributes()];
		
		int idx = 0;
		
		//double[] valsOri  = new double[7]; // 7 statistical features for each channel in original image
		
		for (Integer c : channels) {
			imp.setC(c);
			for (Integer f : statisticalFeatures) {
				switch (f) {
					case 1:
						vals[idx] = imp.getRawStatistics().mean;
						break;
					case 2:
						vals[idx] = imp.getRawStatistics().stdDev/imp.getRawStatistics().mean;
						break;
					case 3:
						vals[idx] = imp.getRawStatistics().stdDev;
						break;
					case 4:
						vals[idx] = imp.getAllStatistics().skewness;
						break;
					case 5:
						vals[idx] = imp.getAllStatistics().kurtosis;
						break;
					default:
						continue;
				}
				idx++;
			}
		}
		
		if (classLabel==-1) {
			vals[vals.length-1] = Utils.missingValue();
		} else {
			vals[vals.length-1] = classLabel;
		}
		data.add(new DenseInstance(1.0, vals));
		return data;
	}
	
	
	public static Roi cropRoi(
			ImagePlus imp, 
			Roi roi
			) {
		
		if (roi==null)
			return null;
		if (imp==null)
			return roi;
		Rectangle b = roi.getBounds();
		int w = imp.getWidth();
		int h = imp.getHeight();
		if (b.x<0 || b.y<0 || b.x+b.width>w || b.y+b.height>h) {
			ShapeRoi shape1 = new ShapeRoi(roi);
			ShapeRoi shape2 = new ShapeRoi(new Roi(0, 0, w, h));
			roi = shape2.and(shape1);
		}
		if (roi.getBounds().width==0 || roi.getBounds().height==0)
			throw new IllegalArgumentException("Selection is outside the image");
		return roi;
	}		
}
