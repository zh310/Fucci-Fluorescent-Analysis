package uk.ac.cam.cruk.fglab;

import fiji.util.gui.GenericDialogPlus;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.Roi;
import ij.plugin.frame.RoiManager;

//import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.scijava.prefs.DefaultPrefService;
//import org.scijava.prefs.PrefService;

public class ParameterDialog {
	
	private static final String helpText = "<html>"
			 +"<h2>Fucci cell fluorescent analysis (ImageJ plugin)</h2>"
			 +" version: 1.6.0<br>"
			 +" date: 2019.01.24<br>"
			 +" author: Ziqiang Huang (Ziqiang.Huang@cruk.cam.ac.uk)<br><br>"
			 +"<h3>Usage:</h3>"
			 +"<&nbsp>Automate cell tracking and fluorescent analysis <br>"
			 +" with Fucci cell lines developed in Fanni Gergely's lab at CRUK-CI.<br>"
			 +" <&nbsp>(contact: Daphne Huberts / Sarah Carden)<br>"
			 +"<br><&nbsp>This script makes use of Trackmate.<br>"
			 +" It take an active tiff image stack as input."
			 +" <&nbsp>User will need to specify the tracking parameters:<br>"
			 +" target channel, average cell radius and so on.<br>"
			 +" <&nbsp>The plugin will use these parameters to build a trackmate model<br>"
			 +" and run it on the given image stack. The result will be displayed.<br>"
			 +" as a ResultTable named \"Fucci cell tracking data table\",<br>"
			 +"  Detected track-spots will be stored as circle ROI in RoiManager.<br>";
	
	// input image paramters
	public ImagePlus image;
	private boolean getActiveImage = false;
	private int activeImgNum = -1;
	private ImagePlus activeImg;
	private String imagePath = "";
	private String imageDirPath = "";
	
	// cell ROI parameters
	public Roi[] cellRois;
	private final String [] roiOptions = new String[] {"StarDist 2D", "LoG Spot"};
	private int roiOption = 0;
	public boolean doStarDist = true;
	public int roiChannel = 3;
	public String cellRoiPath = "";
	public double minCellSize = 15;	//µm²
	private static HashMap<Integer, ArrayList<Integer>> frameROIs;
	
	// TrackMate parameters
	public int targetChannel = 3;	// target channel for spot detection
	public double spotRadius = 10;	// (µm) spot radius for LoG spot detection
	public double qualityThreshold = -1;	// quality threshold for LoG spot detection
	public double linkingMax = -1;	// maximum spot linking / gap closing distance
	public double closingMax = -1;	// maximum spot linking / gap closing distance
	public int frameGap = 4;		// maximum frame gap	
	private double minTrackLength = 240d; // 240 minutes
	
	//	Cell classification parameters
	private final String [] mitosisOptions = 
			new String[] {"Skip detection", "Built-in classifier", "Pre-trained classifiefr", "Train new classifier"};
	private int mitoOption = 0;
	public boolean doWeka = false;
	public String modelPath = "";
	private double minLength = 12; // minimum length of track is 12 hours
	public double minMitoProb = 0.5;	// default mitosis probability threshold 50%
	
	//	Cell similarity analysis parameters
	private double minCost = 150;	// minimum linking cost to be wrong link
	private double maxCount = 1000;
	private double rangeXY = 20; 	// (pixel) search range for similar cells (in pixel and frame unit)
	private double rangeT = 1; 		// (pixel) search range for similar cells (in pixel and frame unit)
	private double minSimScore = 8.0;	// minimum similarity score (between 2 cells)
	
	//	Result saving parameters
	public boolean saveTable = false;
	public boolean saveROI = false;
	public boolean saveSingleCell = false;
	public boolean saveMitosisTrack = false;
	
	public boolean saveModel = true;
	public boolean viewInGUI = true;
	
	public boolean showManager = true;
	public boolean silentMode = false;
	
	private boolean saveWithImg = false;
	public String saveDir = IJ.getDirectory("home");
	
	
	
	// prepare names for image (plus) selection
	public static String[] activeImageList() {
		int numOpenImg = WindowManager.getImageCount();
		String[] titles = new String[numOpenImg+1];
		titles[0] = "open image on disk";
		System.arraycopy(WindowManager.getImageTitles(), 0, titles, 1, numOpenImg);
		return titles;
	}
	
	// get image and image folder path
	private boolean prepareImage () {
		// load data as image plus
		if (this.getActiveImage) {
			this.image = this.activeImg;
			this.image.show(); this.image.getWindow().setVisible(false);
			this.imageDirPath = IJ.getDirectory("image");
			if (this.imageDirPath == null) {
				if (this.saveDir == null) {
					IJ.error("Active image: " + IJ.getImage().getTitle() + " is not associated with a file!");
					return false;
				} else {
					this.imageDirPath = this.saveDir;
					IJ.saveAs(this.image, "Tiff", this.imageDirPath + File.separator + this.image.getTitle());
				}
			}
		} else {
			this.image = IJ.openImage(this.imagePath);
			this.image.show(); this.image.getWindow().setVisible(false);
			this.imageDirPath = IJ.getDirectory("image");
		}
		int[] dims = this.image.getDimensions();	// default order: XYCZT
		if (dims[4] == 1) {
			this.image.setDimensions(dims[2],dims[4],dims[3]);	// set dimension to CTZ, for TrackMate to track in T dimension
		}
		if (!this.imageDirPath.endsWith(File.separator)) this.imageDirPath += File.separator;
		return true;
	}
	
	// get resulting saving folder path
	private void prepareSaveDir () {
		if (this.saveWithImg)	this.saveDir = this.imageDirPath;
		if (!this.saveDir.endsWith(File.separator)) this.saveDir += File.separator;
	}
	
	// get cell ROI array
	private void prepareCellRoi () {
		if (this.doStarDist) {
			StarDistUtility sdr  = new StarDistUtility (this.image, this.roiChannel);
			sdr.setRoiPosition(0, 0, -1); 
			sdr.run(null);
			this.cellRois = sdr.getRois();
		} else {	// or load cell ROI
			this.cellRois = loadPredefinedRois (this.cellRoiPath, this.image, this.minCellSize); //minimum size 15 um2
		}
		//if (cellRois==null) return;	// in the future, add LoG ROI generation part
	}

	// load predefined ROIs 
	private Roi[] loadPredefinedRois(String cellRoiPath, ImagePlus imp, double minRoiSize) {
		if (cellRoiPath==null || !(new File(cellRoiPath)).exists()) return null;
		
		ArrayList<Roi> cellRoiList = ROIUtility.loadRoiList(cellRoiPath);
		if (cellRoiList==null || cellRoiList.size()==0) return null;
		
		for (Roi roi : cellRoiList) {
			imp.setRoi(roi, false);
			if (imp.getStatistics().area <= minRoiSize) {
				cellRoiList.remove(roi);
			}
		}
		int nROI = cellRoiList.size();
		Roi[] cellRoiArray = new Roi[nROI];
		for (int i=0; i<nROI; i++)
			cellRoiArray[i] = cellRoiList.get(i);
		// prepare ROI Manager
		/*
		RoiManager rm = ROIUtility.prepareManager(true);
		rm.runCommand("Open", cellRoiPath);
		if (rm==null || rm.getCount()==0) {
			ROIUtility.restoreManager();
			return null;
		}
		
		ArrayList<Roi> roiList = new ArrayList<Roi>();
		int nROI = rm.getCount();
		
		for (int i=0; i<nROI; i++) {
			imp.setRoi(rm.getRoi(i), false);
			if (imp.getStatistics().area <= minRoiSize) continue;
			Roi r = rm.getRoi(i);
			int t = r.hasHyperStackPosition() ? r.getTPosition() : r.getPosition();
			r.setPosition(0, 0, t);
			roiList.add(r);	
		}
		nROI = roiList.size();
		Roi[] cellROIArray = new Roi[nROI];
		for (int i=0; i<nROI; i++) {
			cellROIArray[i] = roiList.get(i);
		}
		ROIUtility.restoreManager();
		*/
		
		return cellRoiArray;
	}
	// not active
	public boolean trackMateDialog() {
		return true;
	}
	
	private void getParameters() {
		DefaultPrefService prefs = new DefaultPrefService();
		// main parameters
		imagePath = prefs.get(String.class, "Fucci-imgPath", imagePath);
		cellRoiPath = prefs.get(String.class, "Fucci-cellRoiPath", cellRoiPath);
		saveTable = prefs.getBoolean(Boolean.class, "Fucci-saveTable", saveTable);
		saveROI = prefs.getBoolean(Boolean.class, "Fucci-saveROI", saveROI);
		saveModel = prefs.getBoolean(Boolean.class, "Fucci-saveModel", saveModel);
		showManager = prefs.getBoolean(Boolean.class, "Fucci-showManager", showManager);
		viewInGUI = prefs.getBoolean(Boolean.class, "Fucci-viewInGUI", viewInGUI);
		silentMode = prefs.getBoolean(Boolean.class, "Fucci-silentMode", silentMode);
		saveWithImg = prefs.getBoolean(Boolean.class, "Fucci-saveWithImg", saveWithImg);
		saveDir = prefs.get(String.class, "Fucci-saveDir", saveDir);
		// advanced parameters
		roiOption = prefs.getInt(Integer.class, "Fucci-adv-roiOption", roiOption);
		doStarDist = prefs.getBoolean(Boolean.class, "Fucci-adv-doStarDist", doStarDist);
		roiChannel = prefs.getInt(Integer.class, "Fucci-adv-roiChannel", roiChannel);
		minCellSize = prefs.getDouble(Double.class, "Fucci-adv-minCellSize", minCellSize);
		targetChannel = prefs.getInt(Integer.class, "Fucci-adv-targetChannel", targetChannel);
		spotRadius = prefs.getDouble(Double.class, "Fucci-adv-spotRadius", spotRadius);
		qualityThreshold = prefs.getDouble(Double.class, "Fucci-adv-qualityThreshold", qualityThreshold);
		linkingMax = prefs.getDouble(Double.class, "Fucci-adv-linkingMax", linkingMax);
		closingMax = prefs.getDouble(Double.class, "Fucci-adv-closingMax", closingMax);
		frameGap = prefs.getInt(Integer.class, "Fucci-adv-frameGap", frameGap);
		mitoOption = prefs.getInt(Integer.class, "Fucci-adv-mitoOption", mitoOption);
		doWeka = prefs.getBoolean(Boolean.class, "Fucci-adv-doWeka", doWeka);
		modelPath = prefs.get(String.class, "Fucci-adv-modelPath", modelPath);
		minMitoProb = prefs.getDouble(Double.class, "Fucci-adv-minMitoProb", minMitoProb);
		/*
		minCost = prefs.getDouble(Double.class, "Fucci-autoCorrect-minCost", minCost);
		maxCount = prefs.getDouble(Double.class, "Fucci-autoCorrect-maxCount", maxCount);
		rangeXY = prefs.getDouble(Double.class, "Fucci-autoCorrect-rangeXY", rangeXY);
		rangeT = prefs.getDouble(Double.class, "Fucci-autoCorrect-rangeT", rangeT);
		minSimScore = prefs.getDouble(Double.class, "Fucci-autoCorrect-minSimScore", minSimScore);
		*/	
	}
	private void saveParameters() {
		DefaultPrefService prefs = new DefaultPrefService();
		// main parameters
		prefs.put(String.class, "Fucci-imgPath", imagePath);
		prefs.put(String.class, "Fucci-cellRoiPath", cellRoiPath);
		prefs.put(Boolean.class, "Fucci-saveTable", saveTable);
		prefs.put(Boolean.class, "Fucci-saveROI", saveROI);
		prefs.put(Boolean.class, "Fucci-saveModel", saveModel);
		prefs.put(Boolean.class, "Fucci-showManager", showManager);
		prefs.put(Boolean.class, "Fucci-viewInGUI", viewInGUI);
		prefs.put(Boolean.class, "Fucci-silentMode", silentMode);
		
		prefs.put(Boolean.class, "Fucci-saveWithImg", saveWithImg);
		prefs.put(String.class, "Fucci-saveDir", saveDir);
		// advanced parameters
		prefs.put(Integer.class, "Fucci-adv-roiOption", roiOption);
		prefs.put(Boolean.class, "Fucci-adv-doStarDist", doStarDist);
		prefs.put(Integer.class, "Fucci-adv-roiChannel", roiChannel);
		prefs.put(Double.class, "Fucci-adv-minCellSize", minCellSize);
		prefs.put(Integer.class, "Fucci-adv-targetChannel", targetChannel);
		prefs.put(Double.class, "Fucci-adv-spotRadius", spotRadius);
		prefs.put(Double.class, "Fucci-adv-qualityThreshold", qualityThreshold);
		prefs.put(Double.class, "Fucci-adv-linkingMax", linkingMax);
		prefs.put(Double.class, "Fucci-adv-closingMax", closingMax);
		prefs.put(Integer.class, "Fucci-adv-frameGap", frameGap);
		prefs.put(Integer.class, "Fucci-adv-mitoOption", mitoOption);
		prefs.put(Boolean.class, "Fucci-adv-doWeka", doWeka);
		prefs.put(String.class, "Fucci-adv-modelPath", modelPath);
		prefs.put(Double.class, "Fucci-adv-minMitoProb", minMitoProb);
		/*
		prefs.put(Double.class, "Fucci-autoCorrect-minCost", minCost);
		prefs.put(Double.class, "Fucci-autoCorrect-maxCount", maxCount);
		prefs.put(Double.class, "Fucci-autoCorrect-rangeXY", rangeXY);
		prefs.put(Double.class, "Fucci-autoCorrect-rangeT", rangeT);
		prefs.put(Double.class, "Fucci-autoCorrect-minSimScore", minSimScore);
		*/	
	}
	
	public void advDialog() {
		// get stored paramters
		getParameters();
		final Font highlightFont = new Font("Helvetica", Font.BOLD, 12);
		GenericDialogPlus gd = new GenericDialogPlus("FUCCI Fluorescent Analysis Advanced Setup");
		//	file open option group
		String roiMessage = "Cell ROI options:";
			gd.setInsets(10,0,0);
			gd.addMessage(roiMessage, highlightFont);
			gd.setInsets(0,0,0);
			gd.addChoice("Method", roiOptions, roiOptions[roiOption]);
			gd.addNumericField("ROI detection channel", roiChannel, 0);
			gd.addNumericField("Minimum cell size", minCellSize, 1, 6, "µm²");	
		//	trackmate option group
		String trackingMessage = "Cell tracking options:";
			gd.setInsets(10,0,0);
			gd.addMessage(trackingMessage, highlightFont);
			gd.addNumericField("Tracking channel", targetChannel, 0);
			gd.addNumericField("Cell diameter", spotRadius*2, 1, 6, "µm");
			gd.addNumericField("Quality threshold", qualityThreshold, 1, 6, "");
			gd.addNumericField("Linking max distance", linkingMax, 0, 6, "µm");
			gd.addNumericField("Gap closing max distance", closingMax, 0, 6, "µm");
			gd.addNumericField("Max frame gap", frameGap, 0, 6, "frames");
		// result saving option group
		String mitoMessage = "Mitosis detection options:";
			gd.setInsets(10,0,0);
			gd.addMessage(mitoMessage, highlightFont);
			gd.addChoice("Mitosis detection", mitosisOptions, mitosisOptions[mitoOption]);
			gd.addFileField("Pre-trained classifier file", modelPath);
			gd.addSlider("Mitosis probability (%)", 0, 100, minMitoProb*100);
		gd.showDialog();
		if (gd.wasCanceled())	return;
		
		roiOption = gd.getNextChoiceIndex();
		roiChannel = (int) gd.getNextNumber();
		minCellSize = gd.getNextNumber();
		
		targetChannel = (int) gd.getNextNumber();
		spotRadius = gd.getNextNumber()/2; // get radius from diameter
		qualityThreshold = gd.getNextNumber();
		linkingMax = gd.getNextNumber(); // = spotRadius*7;
		closingMax = gd.getNextNumber(); // = spotRadius*5;
		frameGap = (int) gd.getNextNumber();	// default value 4
		
		mitoOption = gd.getNextChoiceIndex();
		modelPath = gd.getNextString();
		minMitoProb = gd.getNextNumber()/100;
		
		// parse dependent parameters
		doStarDist = ( roiOption == 0 );
		if (linkingMax == -1) linkingMax = spotRadius*7;
		if (closingMax == -1) closingMax = spotRadius*5;
		doWeka = ( mitoOption == 0 );
		
		saveParameters();
	}
	
	public boolean mainDialog() {
		// get stored paramters
		getParameters();
		
		final Font highlightFont = new Font("Helvetica", Font.BOLD, 12);
		//final Color highlightColor = Color.BLUE;
		GenericDialogPlus gd = new GenericDialogPlus("FUCCI Fluorescent Analysis");
		//	file open option group
		String fileOpenMessage = "File open options:";
		gd.setInsets(10,0,0);
		gd.addMessage(fileOpenMessage, highlightFont);
		String [] imgTitles = activeImageList();
		gd.setInsets(0,0,0);
		gd.addChoice("Get active image", imgTitles, imgTitles[0]);
		gd.addDirectoryOrFileField("Open image on disk", imagePath, 45);

		//	pre-defined cell ROI (from StarDist 2D)
		gd.addFileField("pre-defined cell ROI", cellRoiPath, 45);
		
		// result saving option group
		String saveMessage = "Result saving options:";
		gd.setInsets(10,0,0);
		gd.addMessage(saveMessage, highlightFont);
		int row = 2, column = 3;
		String[] labels = { "data table", "cell ROI", "TrackMate file", "show ROI Manager", "view in TrackMate", "silent mode" };
		boolean[] states = { saveTable, saveROI, saveModel, showManager, viewInGUI, silentMode };
		gd.setInsets(0,119,0);
		gd.addCheckboxGroup(row, column, labels, states);
		gd.setInsets(0,119,0);
		gd.addCheckbox( "save at image location", saveWithImg );
		gd.addDirectoryOrFileField("or save to", saveDir);
		
		// configuration button
		gd.setInsets(0,119,0);
		gd.addButton("Advance Setup", new ActionListener() { 
		    @Override 
		    public void actionPerformed(ActionEvent ae) { advDialog(); }
		});
		// add help button
		gd.addHelp(helpText);
		gd.showDialog();
		if (gd.wasCanceled())	return false;
		
		activeImgNum = gd.getNextChoiceIndex();
		imagePath = gd.getNextString();
		cellRoiPath = gd.getNextString();
		
		saveTable = gd.getNextBoolean();
		saveROI = gd.getNextBoolean();
		saveModel = gd.getNextBoolean();
		showManager = gd.getNextBoolean();
		viewInGUI = gd.getNextBoolean();
		silentMode = gd.getNextBoolean();
		
		saveWithImg = gd.getNextBoolean();
		saveDir = gd.getNextString();

		// parse dependent parameters
		activeImg = WindowManager.getImage(WindowManager.getNthImageID(activeImgNum));
		getActiveImage = (activeImgNum!=0);
		File imgFile = new File(imagePath);	// parse file and model paths, if not exist, return false
		if (!getActiveImage && !imgFile.exists()) return false;
		File roiFile = new File(cellRoiPath);
		doStarDist = (roiOption==0) && (!roiFile.exists());
		File resultDir = new File(saveDir);	// parse result saving dir, if not exist, return false
		if (!saveWithImg && !resultDir.exists()) return false;
		
		// prepare tetritory parameters
		if ( !prepareImage() ) return false;
		prepareSaveDir();
		prepareCellRoi();

		// save updatd parameters
		saveParameters();
	
		return true;
	}
	
	
	
}
	